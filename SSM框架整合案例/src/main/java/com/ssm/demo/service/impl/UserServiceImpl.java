package com.ssm.demo.service.impl;

import com.ssm.demo.dao.UserMapper;
import com.ssm.demo.model.User;
import com.ssm.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("UserService")
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;

    public User getUserById(int id) {
        return this.userMapper.selectByPrimaryKey(id);
    }

    public int saveUser(User user) {

        return this.userMapper.insert(user);
    }

}
