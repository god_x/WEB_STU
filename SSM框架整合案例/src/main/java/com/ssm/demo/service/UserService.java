package com.ssm.demo.service;

import com.ssm.demo.model.User;

public interface UserService {
    User getUserById(int id);
    int saveUser(User user);
}
