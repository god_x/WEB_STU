package com.ssm.demo.controller;

import com.ssm.demo.model.User;
import com.ssm.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/list/{id}")
    public String list(@PathVariable("id") int id,Model model){
        model.addAttribute(this.userService.getUserById(id));
        return "/user/list";
    }


    @RequestMapping("/add")
    @ResponseBody
    public boolean add(@RequestBody User user){
        return this.userService.saveUser(user)==1;
    }
}
