package com.demo;

public class PeopleFactory {
    public static People getPeopleStatic(String name,int age){
        return new People(name,age);
    }
    public People getPeople(String name,int age){
        return new People(name,age);
    }
}
