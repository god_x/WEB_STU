package com.demo.controller;

import com.demo.People;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/demo")
public class Index {

    @Autowired
    @Qualifier("people1")
    private People people;

    @RequestMapping("/")
    public String index(ModelAndView modelAndView,HttpServletRequest request){
        ApplicationContext ac = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
        modelAndView.addObject("people1",people);
        modelAndView.addObject("people2",ac.getBean("people2"));
        modelAndView.addObject("people3",ac.getBean("people3"));
        modelAndView.addObject("people4",ac.getBean("people4"));
        return "index";
    }
}
