package annotation;

import java.lang.reflect.Method;

/**
 * 权限注解的解析器 获取权限名称
 */
public class PrivilegeAnnotationParse {
    public static String parse(Class targetClass,String methodName){
        String privalege = "";
        try {
           Method method =  targetClass.getMethod(methodName);
           //判断方法上是否有注解
            if(method.isAnnotationPresent(PrivilegeInfo.class)){
                //获取方法上的注解对象
                PrivilegeInfo privilegeInfo =  method.getAnnotation(PrivilegeInfo.class);
                privalege = privilegeInfo.value();
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return privalege;
    }
}
