package service.impl;

import annotation.PrivilegeInfo;
import dao.PersonDao;
import service.PrivilegeService;

/**
 * PrivilegeService实现类
 */
public class PrivilegeServiceImpl implements PrivilegeService{
    private PersonDao personDao;

    public void setPersonDao(PersonDao personDao) {
        this.personDao = personDao;
    }

    @PrivilegeInfo(value = "save")
    public void save() {
        this.personDao.savePerson();
    }

    @PrivilegeInfo(value = "update")
    public void update() {
        this.personDao.updatePerson();
    }

    public void get() {
        this.personDao.getPerson();
    }
}
