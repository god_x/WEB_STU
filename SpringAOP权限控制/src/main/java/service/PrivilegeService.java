package service;

import annotation.PrivilegeInfo;

public interface PrivilegeService {
    /**
     * 该方法需要save权限
     */
    public void save();

    /**
     * 该方法需要update权限
     */
    public void update();

    /**
     * 不需要权限
     */
    public void get();
}
