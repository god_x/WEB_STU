package dao;

public interface PersonDao {
    void savePerson();
    void updatePerson();
    void getPerson();
}
