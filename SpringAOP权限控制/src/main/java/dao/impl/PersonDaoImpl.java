package dao.impl;

import dao.PersonDao;

public class PersonDaoImpl implements PersonDao {

    public void savePerson() {
        System.out.println("save person");
    }

    public void updatePerson() {
        System.out.println("update person");
    }

    public void getPerson() {
        System.out.println("get person");
    }
}
