package privilege;

public class UserPrivilege {
    private String name;

    public UserPrivilege(String name) {
        this.name = name;
    }

    public UserPrivilege() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
