import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import service.PrivilegeService;


public class Test {
    private PrivilegeService privilegeService;
    @org.junit.Test
    public void init(){
        //获取context
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("app.xml");
        //获取服务bean
        privilegeService = (PrivilegeService)applicationContext.getBean("privilegeService");
        //可以通过修改 app.xml文件来修改用户权限
        privilegeService.get();
        privilegeService.save();
        privilegeService.update();
    }
}
