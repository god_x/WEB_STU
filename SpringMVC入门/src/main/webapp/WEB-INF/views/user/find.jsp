<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/8/17 0017
  Time: 8:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>用户查询结果</title>
</head>
<body>
    <table>
        <tr>
            <td>用户名</td>
            <td>${user.username}</td>
        </tr>
        <tr>
            <td>用户密码</td>
            <td>${user.password}</td>
        </tr>
        <tr>
            <td>用户昵称</td>
            <td>${user.nickname}</td>
        </tr>
        <tr>
            <td>用户邮箱</td>
            <td>${user.email}</td>
        </tr>
    </table>
</body>
</html>
