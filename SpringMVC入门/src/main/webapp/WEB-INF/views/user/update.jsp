<%--
  Created by IntelliJ IDEA.
  User: gongbo
  Date: 2017/8/16
  Time: 下午7:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>更新用户信息</title>
</head>
<body>
    <sf:form method="post" modelAttribute="user">
        用户名:<sf:input path="username" value="${user.username}" disabled="true"></sf:input> <sf:errors path="username"></sf:errors> </br>
        用户昵称:<sf:input path="nickname" value="${user.nickname}"></sf:input> <sf:errors path="nickname"></sf:errors> </br>
        用户密码:<sf:password path="password" value="${user.password}"></sf:password> <sf:errors path="password"></sf:errors> </br>
        用户邮箱:<sf:input path="email" value="${user.email}"></sf:input> <sf:errors path="email"></sf:errors> </br>
        <input type="submit" value="添加用户"/> </br>
    </sf:form>
</body>
</html>
