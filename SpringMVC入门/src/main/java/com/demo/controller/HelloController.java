package com.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class HelloController {

    //@RequestMapping 映射路径
    //@RequestParam给controller传值  如果不传则返回400  不带@RequestParam则不会  故在必不可少的参数的时候才用
    //Model参数用于给view传值
    @RequestMapping({"/hello"})
    public String hello(@RequestParam("username") String username,String pass,Model model){
        System.out.println("hello");
        System.out.println(username);
        System.out.println(pass);
        model.addAttribute("username",username);
        //此时用什么作为key？ 默认使用变量的类型名作为key
        model.addAttribute(username);
        return "hello";
    }

    //context参数用于给view传值
    @RequestMapping({"/welcome","/"})
    public String welcome(Map<String,Object> context){
        context.put("test","通过map给view传值");
        return "welcome";
    }

}
