package com.demo.controller;

import com.demo.exception.UserException;
import com.demo.model.User;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/user")
public class UserController {
    private Map<String,User> users = new HashMap<String, User>();

    public UserController(){
        users.put("sdy",new User("sdy","123","宋冬野","asdf"));
        users.put("ldm ",new User("ldm","123","刘东明","asdf"));
        users.put("zyp",new User("zyp","123","周云鹏","asdf"));
        users.put("zww",new User("zww","123","张薇薇","asdf"));
        users.put("wt",new User("wt","123","吴吞","asdf"));
    }

    @RequestMapping(value="/users",method = RequestMethod.GET)
    public String list(Model model){
        model.addAttribute("users",users);
        return "user/list";
    }

    @RequestMapping(value="/add",method = RequestMethod.GET)
    public String add(Model model){
        model.addAttribute(new User());
        return "user/add";
    }

    //@Validated指定验证对象 BindingResult紧跟Validated
    @RequestMapping(value="/add",method = RequestMethod.POST)
    public String add(@Validated User user, BindingResult br, @RequestParam("file")  MultipartFile[] files, HttpSession session) throws IOException{
        System.out.println(br);
        System.out.println(br.getErrorCount());
        if(br.hasErrors())
            return "user/add";
        String path = session.getServletContext().getRealPath("/WEB-INF/classes");
        System.out.println(path);
        //保存上传文件
        for (MultipartFile f:files) {
            if(!f.isEmpty()){       //非空文件
                System.out.println(f.getOriginalFilename());
                FileUtils.copyInputStreamToFile(f.getInputStream(),new File(path+"/"+f.getOriginalFilename()));
            }
        }
        users.put(user.getUsername(),user);
        return "redirect:/user/users";
    }

    @RequestMapping(value="/{username}/update",method = RequestMethod.GET)
    public String update(@PathVariable("username") String username,Model model){
        User user = users.get(username);
        if(user==null)
            return "redirect:/user/users";
        model.addAttribute(user);
        return "user/update";
    }

    @RequestMapping(value="/{username}/update",method = RequestMethod.POST)
    public String update(@PathVariable("username") String username,@Validated User user, BindingResult br){
        if(br.hasErrors())
            return "user/add";
        users.put(username,user);
        return "redirect:/user/users";
    }

    @RequestMapping(value="/{username}/find",method = RequestMethod.GET)
    public String find(@PathVariable("username") String username,Model model){
        User user = users.get(username);
        if(user==null)
            throw new UserException("用户不存在");
        else{
            model.addAttribute(user);
            return "user/find";
        }

    }

    @RequestMapping(value="/{username}/delete",method = RequestMethod.GET)
    public String delete(@PathVariable("username") String username){
        User user = users.remove(username);
        if(user==null)
            throw new UserException("用户不存在");
        else
            return "redirect:/user/users";
    }

    @RequestMapping(value="/login",method = RequestMethod.POST)
    public String login(@RequestParam("username") String username, @RequestParam("password")  String password, HttpSession session, Model model){
        User user = users.get(username);
        if(user==null){
            throw new UserException("用户不存在");
        }else{
            if(user.getPassword().equals(password)){
                session.setAttribute("username",user.getUsername());
                model.addAttribute("users",users);
                return "redirect:/user/users";
            }else{
                throw new UserException("密码错误");
            }
        }
    }

}
